import java.util.ArrayList;
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private ArrayList<String> broadcastKommunikator;
	private ArrayList<Ladung> ladungsverzeichnis;
	private ArrayList<Ladung> ladung = new ArrayList();

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
		
	}
	
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahlNeu) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahlNeu;
	}

	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzentNeu) {
		this.energieversorgungInProzent = energieversorgungInProzentNeu;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzentNeu) {
		this.schildeInProzent = schildeInProzentNeu;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzentNeu) {
		this.huelleInProzent = huelleInProzentNeu;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzentNeu) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzentNeu;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public void addLadung(Ladung neueLadung) {
		this.ladungsverzeichnis.add(neueLadung);	
	}
	
	
//	public void photonentorpedoSchiessen(Raumschiff r) {
//		this.r = r;
//	}
//	
//	public void phaserkanoneSchiessen(Raumschiff r) {
//		this.r = r;
//	}
//	
//	private void treffer(Raumschiff r) {
//		this.r = r;
//	}
//	
//	public void nachrichtAnAlle(String message) {
//		this.nachrichtAnAlle = message;
//	}
//	
//	public void eintraegeLogbuchZurueckgeben() {
//		this.eintraegeLogbuchZurueckgeben = eintraegeLogbuchZurueckgeben;
//	}
//	
//	public void photonentorpedosLaden(int anzahlTorpedos) {
//		this.photonentorpedosLaden = anzahlTorpedos;
//	}
//
//	public void reparaturDurchfuehren(bool schutzschilde, bool energieversorgung, bool schiffshuelle, int anzahlDroiden) {
//		this.reparaturDurchfuehren = reparaturDurchfuehren;
//	}
//	
//	public void zustandRaumschiff() {
//		this.zustandRaumschiff = zustandRaumschiff;
//	}
//	
//	public void ladungsverzeichnisAusgeben() {
//		this.ladungsverzeichnisAusgeben = ladungsverzeichnisAusgeben;
//	}
//	
//	public void ladungsverzeichnisAufraeumen() {
//		this.ladungsverzeichnisAufraeumen = ladungsverzeichnisAufraeumen;
//	}
//	
	
}