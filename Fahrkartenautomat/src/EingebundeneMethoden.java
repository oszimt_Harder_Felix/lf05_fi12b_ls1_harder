import java.util.Scanner;
 
public class EingebundeneMethoden {
 
    public static void main(String[] args) {
    	
    	erneuteBestellung();
    }
    public static void erneuteBestellung() {
		double zuZahlenderBetrag = fahrkartenbestellungErfassen();  
		double fahrkarteBezahlen = fahrkartenBezahl(zuZahlenderBetrag); //meine Main Methode
		fahrkartenAusgeben(); 
		rueckgeldAusgeben(zuZahlenderBetrag, fahrkarteBezahlen);
    }
    
    public static double fahrkartenbestellungErfassen() {	//Methode fahrkartenbestellungErfassen
    	double zuZahlenderBetrag;
    	double anzahlFahrkarte;
        Scanner tastatur = new Scanner(System.in);
		System.out.println("W�hlen Sie ihre Wunschkarte aus: \n"); 
		System.out.println("Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n");
		System.out.println("Tageskarte Regeltarif AB [8,60 EUR] (2)\n");
		System.out.println("Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");
        zuZahlenderBetrag = tastatur.nextDouble();
        if (zuZahlenderBetrag == 1 ) {zuZahlenderBetrag = 2.90;} //1
        if (zuZahlenderBetrag == 2 ) {zuZahlenderBetrag = 8.60;} //2
        if (zuZahlenderBetrag == 3 ) {zuZahlenderBetrag = 23.50;}//3
        System.out.println("Anzahl der Tickets: ");
        anzahlFahrkarte = tastatur.nextDouble();
        if (anzahlFahrkarte <= 0 || anzahlFahrkarte >10) {
        	anzahlFahrkarte = 1;
        	System.out.println("Die Anzahl der Fahrkarten betr�gt jetzt 1, da Sie eine ung�ltige Eingabe get�tigt haben.");
        }
        
        zuZahlenderBetrag = zuZahlenderBetrag * anzahlFahrkarte; 
        return zuZahlenderBetrag;
    }
    
    // Geldeinwurf
    // -----------
   
    
    public static double fahrkartenBezahl(double zuZahlenderBetrag) { //Methode fahrkartenBezahl
    	double eingeworfeneMuenze;
    	double eingezahlterGesamtbetrag;
      Scanner tastatur = new Scanner(System.in);
      eingezahlterGesamtbetrag = 0;
      while(eingezahlterGesamtbetrag < zuZahlenderBetrag) {
   	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
   	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
   	   eingeworfeneMuenze = tastatur.nextDouble();
   	   eingezahlterGesamtbetrag += eingeworfeneMuenze;
      }
      return eingezahlterGesamtbetrag;
    }
        
        // Fahrscheinausgabe
        // -----------------
    
    public static void fahrkartenAusgeben() { // Methode fahrkartenAusgeben
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }
        // R�ckgeldberechnung und -Ausgabe
        // -------------------------------
        
        public static void rueckgeldAusgeben(double zuZahlenderBetrag, double fahrkarteBezahlen) {	//Methode rueckgeldAusgeben
        	double rueckgabebetrag;
        rueckgabebetrag = fahrkarteBezahlen - zuZahlenderBetrag;
        if (rueckgabebetrag > 0.00) 
        {
            System.out.println("Der R�ckgabebetrag in H�he von " + rueckgabebetrag + " EURO");
            System.out.println("wird in folgenden M�nzen ausgezahlt:");
            
            while (rueckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
                System.out.println("2 EURO");
                rueckgabebetrag -= 2.00;
            }
            while (rueckgabebetrag >= 1.00) // 1 EURO-M�nzen
            {
                System.out.println("1 EURO");
                rueckgabebetrag -= 1.00;
            }
            while (rueckgabebetrag >= 0.50) // 50 CENT-M�nzen
            {
                System.out.println("50 CENT");
                rueckgabebetrag -= 0.50;
            }
            while (rueckgabebetrag >= 0.20) // 20 CENT-M�nzen
            {
                System.out.println("20 CENT");
                rueckgabebetrag -= 0.20;
            }
            while (rueckgabebetrag >= 0.10) // 10 CENT-M�nzen
            {
                System.out.println("10 CENT");
                rueckgabebetrag -= 0.10;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
                System.out.println("5 CENT");
                rueckgabebetrag -= 0.05;
            }
        }
 
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                "vor Fahrtantritt entwerten zu lassen!\n" +
                "Wir w�nschen Ihnen eine gute Fahrt.");
        
        erneuteBestellung();
        
 
    }
}
 
//    public static double fahrkartenbestellungErfassen(double zuZahlenderBetrag) {	//Methode fahrkartenbestellungErfassen
//        Scanner tastatur = new Scanner(System.in);
//        System.out.println();
//        System.out.print("Zu zahlender Betrag (EURO): ");
//        zuZahlenderBetrag = tastatur.nextDouble();
//        return zuZahlenderBetrag;
//    }
// 
//    public static double fahrkartenBezahl(double eingezahlterGesamtbetrag, double zuZahlenderBetrag, double eingeworfeneMuenze) { //Methode fahrkartenBezahl
//        Scanner tastatur = new Scanner(System.in);
// 
//        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
//            System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
//            System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
//            eingeworfeneMuenze = tastatur.nextDouble();
//            eingezahlterGesamtbetrag += eingeworfeneMuenze;
//            if (eingeworfeneMuenze > 2) {
//                System.out.println("Bitte werfen sie nur M�nzen im Wert von 2� oder niedriger ein.");
//                zuZahlenderBetrag = eingeworfeneMuenze + zuZahlenderBetrag;
//            }
//            return zuZahlenderBetrag;
//  }
        
     
	
