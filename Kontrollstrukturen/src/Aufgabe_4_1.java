import java.util.Scanner;

public class Aufgabe_4_1 {

	public static void main(String[] args) {
		Scanner scn = new Scanner(System.in);
		System.out.println("Wie lange (in Stunden) hast du gestern geschlafen? ");
		int zahl01 = scn.nextInt();
		System.out.println("Wie lange (in Stunden) hast du heute geschlafen? ");
		int zahl02 = scn.nextInt();
		
		if(zahl01 == zahl02) {
			System.out.println("Zahlen sind gleich! Du hast an beiden Tagen gleich viel geschlafen!"); //Teilaufgabe 2
		}
	
		if(zahl01 < zahl02) {
			System.out.println("Zahlen sind nicht gleich! Du hast heute mehr geschlafen als gestern."); // Teilaufgabe 3
		}
	
		if(zahl01 >= zahl02) {
			System.out.println("Du hast gestern mehr oder gleich so viel geschlafen wie heute."); //Teilaufgabe 4
		}
		
		else {
			System.out.println("Du hattest gestern nicht so viel schlaf.");
			
			
		}
	
	
	}
	
}
