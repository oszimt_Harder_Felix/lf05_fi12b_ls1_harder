import java.util.Scanner;

public class Schleifen_Aufgabe4_Folgen {

	public static void main(String[] args) {
	
		Scanner scn = new Scanner(System.in); //Aufgabe 4: Folgen
		System.out.println("Zahl eingeben: ");
		int zahl = scn.nextInt();
		
//		int n = 2;
//		
//		while(n <= zahl) { 				//Aufgabe c)
//			System.out.println(n);
//			n = n + 4;
//		}
//	}
//}
		
	
		
//		int n = 99;
//		
//			while(n >= zahl) {			 // Aufgabe a)
//			System.out.println(n);
//			n = n - 3;
//		}
		
		
		
//		int n = 2;
//		
//		while(n <= zahl) { 				//Aufgabe e)
//			System.out.println(n);
//			n = n * 2;
//		}

		int n = 1;
		int e = 3;
		while(n <= zahl) {				 // Aufgabe b)
			System.out.println(n);
			n = n + e;
			e = e + 2;
		}
		
}
}
