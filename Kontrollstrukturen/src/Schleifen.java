import java.util.Scanner;

public class Schleifen {

	public static void main(String[] args) {
	
		Scanner scn = new Scanner(System.in); //Aufgabe 1 Schleifen
		System.out.println("Zahl eingeben: ");
		int zahl = scn.nextInt();
		
		int n = 1;
		
		while(n <= zahl) { // while Schleife
			System.out.println(n);
			n = n + 1;
		}
		
		for(int i = 1; i <= zahl; i++) { // for Schleife
			System.out.println(i);
		}
	}

}
